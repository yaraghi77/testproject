<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register')->name('register');
Route::post('login', 'AuthController@login')->name('login');
Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => ''
], function ($router) {
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::post('refresh', 'AuthController@refresh')->name('refresh');
    Route::post('me', 'AuthController@me')->name('me');
    Route::post('payment/gateway', 'PaymentController@createGateway');
    Route::group(['prefix' => 'shop'], function ($router) {
        Route::post('lists', 'ProductController@list');
        Route::post('store', 'ShopController@store');
    });
    Route::group(['prefix' => 'products'], function ($router) {
        Route::post('list', 'ProductController@list');
        Route::post('store', 'ProductController@store');
        Route::get('show/{id}', 'ProductController@show');
    });

    Route::group(['prefix' => 'card'], function ($router) {
        Route::post('add', 'CardController@addToCard');
    });

    Route::resource('bonuscode', 'BonuscodeController');
    Route::resource('order', 'OrderController');

});

