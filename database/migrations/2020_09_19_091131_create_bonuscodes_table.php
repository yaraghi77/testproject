<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonuscodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonuscodes', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->date('start');
            $table->date('expire');
            $table->integer('max_promotion');
            $table->integer('percent');
            $table->integer('price');
            $table->integer('user_id');
            $table->enum("type", ['percent','price'])->default('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonuscodes');
    }
}
