<?php

namespace App\Facades;



use Illuminate\Support\Facades\Facade;



class CalculateClass extends Facade{



    protected static function getFacadeAccessor() { return 'calculate'; }

}
