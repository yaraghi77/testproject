<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonuscode extends Model
{
    protected $fillable = ['code','percent','type','max_promotion','expire','start','user_id','price'];

}
