<?php



namespace App\Http\Calculate;


class CalculateClass {



    public static function orderPrice($order,$bonusCode = null){
        $products_price = 0 ;
        foreach ($order->products as $product){$products_price += $product->price ;}

        if(isset($bonusCode->id)){
            switch ($bonusCode->type) {
                case "percent":
                    $bonus = ($bonusCode->percent / 100) * $products_price;
                    if($bonus > $bonusCode->max_promotion){
                        $products_price -= $bonusCode->max_promotion ;
                    }else{
                        $products_price -= $bonus ;
                    }
                    break;
                case "price":
                    $bonus = $bonusCode->price;
                    if($bonus > $bonusCode->max_promotion){
                        $products_price -= $bonusCode->max_promotion ;
                    }else{
                        $products_price -= $bonus ;
                    }
                    break;
                default:
                    //noting
            }
        }
        return $products_price;



    }

}
