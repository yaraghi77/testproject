<?php

namespace App\Http\Controllers;

use App\Product;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    public function __construct(Product $Product,Shop $Shop)
    {
        $this->user_id = Auth::user()->getAuthIdentifier();
        $this->Shop = $Shop ;
        $this->Product = $Product ;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $this->validate($request, [
            'lat' => 'required|numeric|between:0,99.99',
            'long' => 'required|numeric|between:0,99.99',
        ]);
        $lat = $request->lat ;
        $long = $request->long ;
        $results = $this->DB::table("shops")->select("*"
            ,$this->DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
        * cos(radians(shops.lat)) 
        * cos(radians(shops.long) - radians(" . $long . ")) 
        + sin(radians(" .$lat. ")) 
        * sin(radians(shops.lat))) AS distance"))
            ->orderBy("distance")
            ->get();
        return $results;

    }
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|string',
            'lat' => 'required|numeric|between:0,99.99',
            'long' => 'required|numeric|between:0,99.99',
        ]);
        $shop = $this->Shop::create([
            'title' => $request->title,
            'lat' => $request->lat,
            'long' => $request->long,
            'user_id' =>$this->user_id,
        ]);
        if ($shop) {
            return response()->json([
                'message' => "Shop Created",
                'product' => $shop
            ]);
        } else {
            return response()->json([
                'message' => "something wrong"
            ],404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function edit(Shop $shop)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop $shop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {
        //
    }
}
