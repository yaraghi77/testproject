<?php

namespace App\Http\Controllers;

use App\Product;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Location;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    public function __construct(DB $DB, Product $Product, Shop $Shop)
    {
        $this->DB = $DB;
        $this->Product = $Product;
        $this->Shop = $Shop;
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|int',
            'shop_id' => 'required|int',
        ]);
        $shop = $this->Shop::findOrFail($request->shop_id);
        $product = $this->Product::create([
            'title' => $request->title,
            'price' => $request->price,
            'description' => $request->description,
            'user_id' => $this->user->getAuthIdentifier(),
            'shop_id' => $shop->id,
        ]);
        if ($product) {
            return response()->json([
                'message' => "Product Created",
                'product' => $product
            ]);
        } else {
            return response()->json([
                'message' => "something wrong"
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    public function list(Request $request)
    {
        $this->validate($request, [
            'shop_id' => 'required|int',
        ]);
        $products = $this->Shop::with('products')->findOrFail($request->shop_id);
        return $products;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
