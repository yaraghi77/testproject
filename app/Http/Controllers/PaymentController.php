<?php

namespace App\Http\Controllers;

use App\Bonuscode;
use App\Http\Calculate\CalculateClass;
use App\Order;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function __construct(Payment $Payment, Order $order, Bonuscode $bonuscode, CalculateClass $calculateClass)
    {
        $this->Order = $order;
        $this->Bonuscode = $bonuscode;
        $this->CalculateClass = $calculateClass;
        $this->Payment = $Payment;
        $this->user = Auth::user();
    }

    public function createGateway(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|int',
            'bonus_code' => 'string',
            'gateway_type' => 'required|in:saman,mellat',
        ]);
        $order_id = $request->order_id;
        $bonus = null;
        if (isset($request->bonus_code)) {
            $bonus = $this->Bonuscode::where('code', $request->bonus_code)->first();
        }
        $order = $this->Order::with('products')->findOrFail($order_id);
        $price = $this->CalculateClass::orderPrice($order, $bonus);
        if ($price) {
            $payment = $this->Payment::create([
                'order_id' => $order->id,
                'user_id' => $this->user->getAuthIdentifier(),
                'type' => $request->gateway_type,
                'price' => $price,
                'status' => 0,
                'reference' => rand(10000, 1000000),
                'autotrain' => rand(10000, 1000000)
            ]);
        }else{
            return response()->json([
                'message' => "something wrong"
            ],400);
        }

        return $payment;
    }

}
