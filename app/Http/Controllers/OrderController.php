<?php

namespace App\Http\Controllers;

use App\Card;
use App\order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct(Product $Product, Card $Card)
    {
        $this->Product = $Product;
        $this->Card = $Card;
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            //'products' => 'required|array|min:1',
        ]);
        $user_id = $this->user->id;
        $card = $this->Card::where('user_id', $user_id)->whereHas('product')->with('product')->get();
        if (!empty($card)) {
            $products = [];
            foreach ($card as $key => $product) {
                $products[$key] = $product->product->id;
            }

            $order = Order::create(['status' => 0, 'user_id' => $user_id]);
            $order->products()->attach($products);
            if ($order) {
                return response()->json([
                    "order" => $order ,
                    "products" => $order->products
                ]);
            } else {
                return response()->json([
                    'message' => "something wrong"
                ],400);
            }
        }else{
            $orders = Order::where('user_id',$user_id)->where('status',0)->latest('updated_at')->first();
            return response()->json([
                'message' => "you cards empty , check last order" ,
                'orders' => $orders ,
            ],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function show(order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        //
    }
}
