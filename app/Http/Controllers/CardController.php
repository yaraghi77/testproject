<?php

namespace App\Http\Controllers;

use App\Card;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CardController extends Controller
{
    public function __construct(Product $Product,Card $Card,Auth $Auth)
    {
        $this->Product = $Product ;
        $this->Card = $Card ;
        $this->User = $Auth::user() ;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCard(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|int',
        ]);
        $product = $this->Product::findOrFail($request->product_id);
        if(isset($request->count)){$count = $request->count;}else{$count = 1;}
        $this->Card->where('product_id',$product->id)->where('user_id',$this->User->id)->delete();
        $card = $this->Card::create([
            'product_id' => $product->id,
            'user_id' => $this->User->id,
            'count' => $count
        ]);

        if ($card) {
            return response()->json([
                'message' => "Added",
                'product' => $card
            ]);
        } else {
            return response()->json([
                'message' => "something wrong"
            ]);
        }
    }
    public function remove(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|int',
        ]);

    }
}
