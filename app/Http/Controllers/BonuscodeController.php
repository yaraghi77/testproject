<?php

namespace App\Http\Controllers;

use App\Bonuscode;
use App\Card;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BonuscodeController extends Controller
{
    public function __construct(Auth $Auth)
    {

        $this->user = $Auth::user() ;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'start' => 'required|date|after:today',
            'expire' => 'required|date|after_or_equal:start_date',
            'max_promotion' => 'required|int',
            'percent' => 'required|int',
            'price' => 'required|int',
            'type' => 'required|in:Price,Percent',

        ]);
        $Bonuscode = Bonuscode::create([
            'code' => $request->code,
            'type' => $request->type,
            'start' => $request->start,
            'expire' => $request->expire,
            'max_promotion' => $request->max_promotion,
            'percent' => $request->percent,
            'price' => $request->price,
            'user_id' => $this->user->getAuthIdentifier(),
        ]);
        if ($Bonuscode) {
            return response()->json([
                'message' => "Created"
            ]);
        } else {
            return response()->json([
                'message' => "something wrong"
            ],404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bonuscode  $bonuscode
     * @return \Illuminate\Http\Response
     */
    public function show(Bonuscode $bonuscode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bonuscode  $bonuscode
     * @return \Illuminate\Http\Response
     */
    public function edit(Bonuscode $bonuscode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bonuscode  $bonuscode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bonuscode $bonuscode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bonuscode  $bonuscode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bonuscode $bonuscode)
    {
        //
    }
}
