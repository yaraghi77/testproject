<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class RoleAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if($user){
            if(!$user->hasRole('safir')){
                $response = [
                    'status' => 2,
                    'message' => 'you are not driver',
                ];
                return response()->json($response, 413);
            }else{
                return $next($request);
            }
        }else{
            $response = [
                'status' => 2,
                'message' => 'user not found',
            ];
            return response()->json($response, 404);
        }


    }
}
