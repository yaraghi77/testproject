<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title','price','lat','long','user_id','shop_id','description'];
    public function order()
    {
        return $this->belongsToMany('App\Order');
    }
}
