<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['status','user_id'];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
    public function Payment()
    {
        return $this->hasMany('App\Payment');
    }
}
