<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{

    protected $fillable = ['title','lat','long','user_id'];
    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
