<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    protected $fillable = ['order_id','user_id','price','type','currency','status','reference','autotrain'];

}
